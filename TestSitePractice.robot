*** Settings ***
Resource            ./resources/Resource.robot
Resource            ./resources/PO/Home.robot
Resource            ./resources/PO/Carrinho.robot
Test Setup          Open the browser
Test Teardown       Fechar navegador

*** Test Case ***
Caso de Teste com PO 01: Remover Produtos do Carrinho
    Home.Acessar a página home do site
    Home.Adicionar o produto "t-shirt" no carrinho
    Carrinho.Excluir o produto do carrinho
    Carrinho.Conferir se o carrinho fica vazio
